using System;

namespace DN.Games
{
    /// <summary>
    /// Настройки уровня
    /// </summary>
    [Serializable]
    public class LevelSettings
    {
        /// <summary>
        /// Ширина поля в ячейках
        /// </summary>
        public int Width = 10;

        /// <summary>
        /// Высота поля в ячейках
        /// </summary>
        public int Height = 10;

        /// <summary>
        /// Начальная длинна змейки
        /// </summary>
        public int StartSnakeWidth = 3;

        /// <summary>
        /// Скорость передвижения змейки
        /// </summary>
        public float Speed = 1.0f;
    }
}