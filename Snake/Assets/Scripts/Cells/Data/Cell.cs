using DN.Games;
using System;
using UnityEngine;

namespace DN.Cells
{
    /// <summary>
    /// Ячейка игрового поля
    /// </summary>
    public class Cell
    {
        /// <summary>
        /// Позиция ячейки (совпадает с индексами в массиве ячеек)
        /// </summary>
        public Vector2Int Position;

        /// <summary>
        /// Содержимое ячейки если есть
        /// </summary>
        public ICellContent Content;

        public Cell(int xCoodinate, int yCoodinate)
        {
            Position.x = xCoodinate;
            Position.y = yCoodinate;
        }        

        public void SetContent(ICellContent contentObject)
        {
            Content = contentObject;
        }

        public void RemoveContent()
        {
            Content = null;
        }
    }
}