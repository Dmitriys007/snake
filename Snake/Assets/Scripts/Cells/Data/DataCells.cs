using DN.Games;

namespace DN.Cells
{
    /// <summary>
    /// Игровое поле с ячейками
    /// </summary>
    public class DataCells
    {
        public Cell[,] cells { get; private set; }
        public LevelSettings settings { get; private set; }

        public DataCells(LevelSettings levelSettings)
        {
            settings = levelSettings;
            cells = new Cell[settings.Width, settings.Height];
            for (var i = 0; i < settings.Width; i++)
                for (var j = 0; j < settings.Height; j++)
                    cells[i, j] = new Cell(i, j);
        }
    }
}