using UnityEngine;

namespace DN.Cells
{
    /// <summary>
    /// Визуализация игрового поля
    /// </summary>
    public class VisualArea : MonoBehaviour
    {
        [SerializeField] private VisualCell visualCellPrefab = null;

        public void Init(DataCells cellsData)
        {
            for (var i = 0; i < cellsData.settings.Width; i++)
                for (var j = 0; j < cellsData.settings.Height; j++)
                    Instantiate(visualCellPrefab, new Vector3(j, i, 0), Quaternion.identity, transform).Init(cellsData.cells[i, j]);
        }
    }
}