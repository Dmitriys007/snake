using DN.Cells;
using UnityEngine;

namespace DN.Games
{
    public class Game : MonoBehaviour
    {
        /// <summary>
        /// Настройки уровня
        /// </summary>
        [SerializeField] private LevelSettings levelSettings = new LevelSettings();
        [SerializeField] private VisualArea visualAreaPrefab = null;
        [SerializeField] private Snake snakePrefab = null;
        [SerializeField] private FoodSpawner foodSpawner = null;

        /// <summary>
        /// Данные по игровому полю
        /// </summary>
        private DataCells dataCells;

        private void Start()
        {
            // Инициировать данные
            dataCells = new DataCells(levelSettings);

            // Инициировать визуальную часть
            Instantiate(visualAreaPrefab).Init(dataCells);

            // Инициировать логику
            Instantiate(snakePrefab)
                .Init(dataCells)
                .OnDieAction += OnSnakeDie;
            Instantiate(foodSpawner).Init(dataCells);
        }

        private void OnSnakeDie()
        {
            Debug.Log("GAME OVER CALL");
        }
    }
}