namespace DN.Games
{
    /// <summary>
    /// ��������������� �� ������� ������-������
    /// </summary>
    public interface ISnake
    {
        /// <summary>
        /// ����� ���� ������
        /// </summary>
        void Die();

        /// <summary>
        /// ��������� ���� ������
        /// </summary>
        void AplyFood();
    }

    /// <summary>
    /// ������������� � ������� ������� (������ ��� �������� ������ ��� ���)
    /// </summary>
    public interface ICellContent
    {
        void OnSnakeComin(ISnake snake);

    }
}