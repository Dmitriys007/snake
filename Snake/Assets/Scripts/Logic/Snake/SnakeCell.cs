using DN.Cells;
using UnityEngine;

namespace DN.Games
{
    /// <summary>
    /// Визуальный сегмент змейки
    /// </summary>
    public class SnakeCell : MonoBehaviour, ICellContent
    {
        private Cell parentCell = null;

        public void Init(Cell parent)
        {
            MoveToCell(parent);
        }

        public void MoveToCell(Cell target)
        {
            parentCell?.RemoveContent();         
            parentCell = target;
            parentCell.SetContent(this);
            transform.position = new Vector3(parentCell.Position.x, parentCell.Position.y, -0.1f);
        }

        public void OnSnakeComin(ISnake snake)
        {
            snake.Die();//Змейка сама себя убивает при столкновении
            Debug.Log("Obstacle snake vs snake!");
        }
    }
}