using DN.Cells;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace DN.Games
{
    /// <summary>
    /// Змейка игрока
    /// </summary>
    public class Snake : MonoBehaviour, ISnake
    {
        /// <summary>
        /// Гибель змейки, GAME OVER
        /// </summary>
        public Action OnDieAction;

        /// <summary>
        /// Визуальное отображение сегмента
        /// </summary>
        [SerializeField] private SnakeCell visualBodyPrefab = null;

        /// <summary>
        /// Данные по ячейкам
        /// </summary>
        private DataCells data = null;

        /// <summary>
        /// Тело змейки (сегменты в ячейках)
        /// </summary>
        private List<Cell> body = new List<Cell>();

        /// <summary>
        /// Текущее направление движения
        /// </summary>
        private Vector2Int moveVector = new Vector2Int(1,0);

        /// <summary>
        /// Таймер до следующего хода
        /// </summary>
        private float timer = 0.0f;

        private void Update()
        {
            if (Input.GetKey(KeyCode.W))
                moveVector = new Vector2Int(0, 1);
            if (Input.GetKey(KeyCode.S))
                moveVector = new Vector2Int(0, -1);

            if (Input.GetKey(KeyCode.D))
                moveVector = new Vector2Int(1, 0);

            if (Input.GetKey(KeyCode.A))
                moveVector = new Vector2Int(-1, 0);

            if (timer < Time.realtimeSinceStartup)
            { 
                timer = Time.realtimeSinceStartup + data.settings.Speed;
                Move();
            }
        }

        private void Move()
        {
            Vector2Int newMoving = body[body.Count - 1].Position + moveVector; // позиция следующей занимаемой ячейки

            if (newMoving.x < 0 || newMoving.x >= data.settings.Width || newMoving.y < 0 || newMoving.y >= data.settings.Height) // проверка позиции на выход из диапазона
            {
                //GAME OVER
                Die();
                return;
            }

            if (data.cells[newMoving.x, newMoving.y].Content != null)
            {
                data.cells[newMoving.x, newMoving.y].Content.OnSnakeComin(this);
                return;
            }
            
            Cell movingData = body[0];
            body.RemoveAt(0);
            body.Add(data.cells[newMoving.x, newMoving.y]);
            (movingData.Content as SnakeCell).MoveToCell(data.cells[newMoving.x, newMoving.y]);
        }

        public Snake Init(DataCells dataCells)
        {
            Debug.Log("Init logic call");
            data = dataCells;            
            for (var i = 0; i < data.settings.StartSnakeWidth; i++)
            {
                body.Add(data.cells[i, 0]);                
                ShowSegment(data.cells[i, 0]);
            }
            return this;
        }

        public void ShowSegment(Cell cell)
        {
            Instantiate(visualBodyPrefab, new Vector3(cell.Position.x, cell.Position.y, -0.1f), Quaternion.identity, transform).Init(cell);
        }

        public void Die()
        {
            OnDieAction?.Invoke();
            Destroy(gameObject);
        }

        public void AplyFood()
        {
            Vector2Int newMoving = body[body.Count - 1].Position + moveVector; // позиция следующей занимаемой ячейки
            body.Add(data.cells[newMoving.x, newMoving.y]);            
            ShowSegment(data.cells[newMoving.x, newMoving.y]);
        }
    }
}