using DN.Cells;
using System.Collections;
using UnityEngine;

namespace DN.Games
{
    /// <summary>
    /// �������� ��� �� �������
    /// </summary>
    public class FoodSpawner : MonoBehaviour
    {
        [SerializeField] private Food foodPrefab;
        [SerializeField] private float timeSpawn = 3.0f;
        private float timer = 0;


        private DataCells data = null;

        public void Init(DataCells dataArea)
        {
            data = dataArea;
            StartCoroutine(CreateFood());
        }

        //todo: ������������� ����� ��������� ������ ��� ������ ���� ��������� ������
        private IEnumerator CreateFood()
        {
            yield return null;
            int i = Random.Range(0, data.settings.Width);
            int j = Random.Range(0, data.settings.Height);

            while (true)
            {
                timer -= Time.deltaTime;
                if (timer < 0)
                {
                    if (data.cells[i, j].Content == null)
                    {
                        Instantiate(foodPrefab, transform).Init(data.cells[i, j]);
                        timer = timeSpawn;
                    }

                    i = Random.Range(0, data.settings.Width);
                    j = Random.Range(0, data.settings.Height);
                }
                yield return null;
            }
        }
    }
}