using DN.Cells;
using UnityEngine;

namespace DN.Games
{
    public class Food : MonoBehaviour, ICellContent
    {
        private Cell parentCell = null;

        public void Init(Cell cell)
        {
            parentCell = cell;
            transform.position = new Vector3(cell.Position.x, cell.Position.y, -0.1f);
            cell.SetContent(this);
        }

        public void OnSnakeComin(ISnake snake)
        {
            parentCell.RemoveContent();
            snake.AplyFood();
            Destroy(gameObject);
        }
    }
}